+++
date = "2017-02-02"
title = "The Communications Merit Badge"
description = "A short page which fulfills the communications merit badge blog requirement"
categories = ["errata"]
tags = [""]
draft = false
+++

# The Communications Merit Badge
This article is here to satisfy requirement 7b of the Communications Meritbadge for the BSA.

## This is an Offsite Link
[Google, WOW!](https://google.com)
I even did you the favor of directing you to the **HTTPS** version instead of forcing you through a redirect.

## This is a Picture
<img
data-sizes="auto"
data-src="/images/boston-dynamics-lineup.jpg"
class="lazyload"
width="100%"
>
This is Boston Dynamics' lineup, it's pretty cool.

Wow, what an article. Until nextime!