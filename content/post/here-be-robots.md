+++
date = "2016-11-01"
title = "Here Be Robots"
draft = false
description = "The short tale of an autonomous car"
tags = ["autonomous", "arduino"]
categories = ["robotics"]
+++

Here Be Robots
==============

The Short of It
---------------

I really like the idea of autonomy, so I am working on a simple(ish?) platform to run tests on. It's kinda cool. Remember that scene from Apollo 13 where one of the engineers says "We need to make this fit into the hole for this using nothing but that"; this will definitely be easier than that. So, what do we have to work with? I went through my desk and my basement and found out I have about 7.23 times what I need (I measured). The short of it is I need to make some stuff into some other stuff.
Shouldn't be too hard.

The Less Short of It
--------------------

What should any well thought out plan include? Glad you asked... I don't know. I just jumped straight into it and figured as long as I didn't blow up too much stuff I would be fine. First things first. Hook up Arduino pin 9 to ESC PWM, create common ground, write 7 lines of code and fire it up. I figured this should work well enough as it is what lots of people suggest doing. What I got was a reasonably large (1.5x2 ft) RC car running full tilt into the woods. What was the cause? Well, if you would direct your attention at exhibit one of the Arduino servo library source at this time, thank you very much.

**Arduino/libraries/Servo/src/Servo.h**
```cpp
#define MIN_PULSE_WIDTH       544     // the shortest pulse sent to a servo  
#define MAX_PULSE_WIDTH      2400     // the longest pulse sent to a servo 
#define DEFAULT_PULSE_WIDTH  1500     // default pulse width when servo is attached
```

First off, I don't understand why the comment writer feels they must reiterate what MIN_PULSE_WIDTH, MAX_PULSE_WIDTH and DEFAULT_PULSE_WIDTH mean, but don't feel they ought to identify the units used by those variables (It's &mu;s by the way). Lets get back on topic shall we? Notice the MIN, MAX and DEFAULT pulse widths? Well they are ok for servos, but thats about it. Most ESCs accept RC standard PWM signals, meaning 1000 - 2000 &mu;s mapping too zero - full throttle; mine does not. Unfortunately for me, it turned out the obvious fix (Setting the range when `attach`ing the servo) did not work. Not only was the PWM spectrum different from that assumed by the Servo library, it was **WAY** different. The PWM range on my ESC is both partitioned differently (Part of its spectrum is for reverse) and non-compliant with the RC standard. I hooked the RC receiver up to my oscilloscope to see what was normally sent to the ESC and got pulse widths of 900 &mu;s full reverse, 1280 &mu;s neutral and 2250&mu;s full forward. Once I started using the Servo library to directly write &mu;s, and used the correct values, the throttle was working fine. At this point I decided to get some OJ and call it a day. It isn't a robot yet but we are making some progress. Thanks for the runaway car DEFAULT_PULSE_WIDTH.