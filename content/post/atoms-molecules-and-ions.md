+++
date = "2017-01-21"
title = "Atoms, Molecules and Ions"
description = "A short set of notes on the history and basic laws of Atoms, Molecules and Ions"
draft = false
categories = ["notes"]
tags = ["chemistry"]
+++

<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js"></script>
<script
src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/config/TeX-MML-AM_SVG.js"></script>

Atoms, Molecules, and Ions
==========================

History
-------
* The Greeks
    * By 400 B.C. the Greeks believed that all matter was composed of a combination of four fundemental substances. These substances were fire, water, earth, and air.
    * They were split as to whether matter could be infinitely subdivided into component pieces, or whether there was a fundemental set of particles.
    * The two primary supporters of the fundemental particle theory were Demokritos and Leucippos, who termed these fundemental paticles **atomos**.
* Alchemy
    * For the next 2000 years, the pseudoscience of **alchemy** reigned supreme.
    * Alchemists were a mixed bunch, with some being total cranks, and some pursuing serious scientific inquiry.
    * Alchemists discovered several elements, and learned to produce mineral acids.
* Modern Chemistry
    * Georg Bauer and Paracelsus laid the foundation for modern chemistry by developing methods for extracting metals from ores, and for using minerals for medicinal purposes.
    * Robert Boyle carried out the first known quantitative experiments, measuring the relationship between the pressure and volume of gases.
    * Oxygen was discovered by Joseph Priestley, and was initially believed to be air devoid of phlogiston, the substance believed to inhibit combustion.
    * **Antoine Lavoisier** conducted experiments which supported the theory of **conservation of mass**, stating that mass is never created nor destroyed.
    * **Joseph Proust** brought forward the **law of definite porportion**, stating that a given compound has a fixed proportion of elements by mass.
    * **John Dalton** proposed the **law of multiple porportions**, stating that when two elements form a series of compounds, the ratio of the masses of the second element that combine with 1 gram of the first element can always be reduced to whole numbers. This is obviously a result of the fact that whole atoms must be combined when forming compounds.

        |           |Mass of Nitrogen That Combines with 1g of Oxygen|Ratio to Compound A|
        |-----------|------------------------------------------------|-------------------|
        |Compound A |1.750g                                          |1:1                |
        |Compound B |0.8750g                                         |1:2                |
        |Compound C |0.4375g                                         |1:4                |
        
        Table: Table Demonstrating Law of Multiple Porportions

    * Dalton later published his atomic theory, in which he listed the following key points:
        * Each element is composed of atoms.
        * The atoms of a given element are identical, and differ between each element in some fundemental way.
        * Compounds are formed through the combination of differing types of atoms.
        * Chemical reactions are reorganization of atoms, and do not fundementally change the individual atoms.
    * **Amadeo Avogadro** interpreted the results of earlier experiementation by **Joseph Gay-Lussac**. He postulated that at a given temperature and pressure, equal volumes of different gases contain the same number of particles. For example: \\(2 Volumes H_2 + 1 Volume O_2 \rightarrow 2 Volumes H_2O\\) is equivalent to \\(2H_2 + O_2 \rightarrow 2H_2O\\). Interestingly, his hypothesis was not accepted for half a century.
    * J&ouml;ns Jakob Berzelius discovered cerium, selenium, silicon and thorium. He also created the modern symbolism used in writing the formulas of compounds.
    * **J. J. Thomson** used cathode-ray tubes and a magnetic field to determine the ratio of an electrons charge to its mass, that ratio being \\(\frac{e}{m}=-1.76*10^8\frac{Coulombs}{gram}\\). Thompson eventaully came up with his model for the atom, called the **plum pudding model**, it characterizes the atom as a positively charged cloud with negatively charged electrons embeded in it with a random distribution.
    * **Robert Millikan** preformed his **oil drop** experiment, with which he was able to determine the mass of the electron as \\(9.11*10^{-31}kilograms\\).
    * **Ernest Rutherford** disproved the plum pudding model with his \\(\alpha\\) particle experiment, in which he projected a beam of \\(\alpha\\) particles at a sheet of foil, and observed significant scattering. This result meant the majority of the mass of an atom must be in a high density center. Rutherford proposed his **orbital model** of the atom, in which electrons orbited a dense nucleus.

Short Notes on Ions Because It Says So in the Title
---------------------------------------------------
* Positively charged ions are cations.
* Negatively charged ions are anions.
* Very notes much wow.