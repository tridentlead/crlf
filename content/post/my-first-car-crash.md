+++
draft = true
description = "The rest of the short tale of an autonomous car"
tags = ["autonomous","nodejs"]
categories = ["robotics"]
date = "2016-11-05"
title = "My First Car Crash"
+++

What's good
===========

Well today was fun. Since my last post I got the model car working, really well in fact (I will probably post a video later). I made an enclosure, neatened up the cables and wrote a nice web client to control the car. In the end, it looked pretty good, a situation which lasted entire moments.

The Event
---------

I decided to take the thing to MIT and test it out with one of my friends, which I did. It worked great in the halls, but it was moving a bit fast. I decided a throttle control would be cool so I added one, pretty simple, it just changed between 3 steps of PWM widths. I really wanted to keep it in one piece so I figured I would only run the thing outside for the time-being. This thing's top speed must have been at least 20MPH, likely higher. Only one problem, halfway through it lost connection for a few seconds. Now, I had a kill switch set up just incase something like this happened, it was very complicated:

```javascript
    socket.on("disconnect", () => {
        arduino.servoWrite(ESC, 1200)
        arduino.servoWrite(STE, 1590)
        arduino.reset()
    })
```

The variables are reasonably self-evident, **ESC** is for the speed controller and **STE** is for the steering. When the connection was lost, this elegant piece of code kicked in. Normally this would be a good thing, right? In my case, the steering servo went kinda wonky, and the thing crashed into a light pole. Now, cars are designed with a crumple zone, which is designed to break before you do in the event of a crash. Apparently my chassis was designed by an automotive engineer, because it seemed to have a crumple zone too. In a real car, the crumple zone is most of the first third of the car, which is good because the important stuff is in the middle third. My car also has a crumple zone, unfortunately, it is a little more accurate than I would like. After the impact my car looked a bit more like a smoothie.
Yeah, not fun.

The Aftermath
-------------

First though on seeing that: I am screwed. Second thought on seeing that: Epoxy.

Epoxy is great, and it almost always works. The problems start when it doesn't. I have a slight suspicion that when the oil filled shocks ruptured, the oil made a fine layer on the surfaces I was trying to epoxy, which prevented the epoxy from bonding correctly. In any case, I could not get the epoxy to work. I am also missing **MANY** small plastic fragments which ended up all over kingdom-come. At this point I think my best bet is to 3D print an entirely new chassis of my own design. I do still have to design it though. I will keep everyone posted via this blog and we'll just have to see what I come up with.
